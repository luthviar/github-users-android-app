package com.devwithluthviar.githubusersapp

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject

class FavoriteActivity : AppCompatActivity() {

    companion object {
        const val TOKEN_AUTH_GITHUB = "token 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"
    }
    private lateinit var rvHeroes: RecyclerView
    var usersResponse = ArrayList<User>()
    val loading = LoadingDialog(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)
        rvHeroes = findViewById(R.id.rv_heroes)
        rvHeroes.setHasFixedSize(true)
        supportActionBar?.elevation = 0f

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Favorite User"
        loading.startLoading()

        val mainViewModel = obtainViewModel(this@FavoriteActivity)
        mainViewModel.getAllUserdb().observe(this, { userdbList ->
            if (userdbList != null) {
                if(userdbList.count() < 1) {
                    Toast.makeText(this@FavoriteActivity, "No Favorite Data, please add one first.", Toast.LENGTH_SHORT).show()
                }
                usersResponse.clear()
                for ((index, value) in userdbList.withIndex()) {
                    val username = value.username
                    val name = value.name
                    val avatar = value.avatar
                    val company = value.company
                    val location = value.location
                    val repository = value.repository
                    val follower = value.follower
                    val following = value.following

                    val lastUser = User(
                        username = username!!,
                        name = name!!,
                        avatar = avatar!!,
                        company = company!!,
                        location = location!!,
                        repository = repository!!,
                        follower = follower!!,
                        following = following!!
                    )
                    usersResponse.add(lastUser)
                }
                loading.isDismiss()
                showRecyclerList(usersResponse)
            }
        })
    }

    private fun obtainViewModel(activity: AppCompatActivity): MainViewModelUserdb {
        val factory = UserdbViewModelFactory.getInstance(activity.application)
        return ViewModelProvider(activity, factory).get(MainViewModelUserdb::class.java)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showRecyclerList(listUserData: ArrayList<User> = ArrayList()) {
        if (applicationContext.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            rvHeroes.layoutManager = GridLayoutManager(this, 2)
        } else {
            rvHeroes.layoutManager = LinearLayoutManager(this)
        }
        var listUserAdapter = ListUserAdapter(ArrayList(), this)
        if(listUserData.count() < 1) {
            rvHeroes.visibility = View.GONE
        } else {
            rvHeroes.visibility = View.VISIBLE
            listUserAdapter = ListUserAdapter(listUserData, this)
            rvHeroes.adapter = listUserAdapter
        }
        listUserAdapter.setOnItemClickCallback(object : ListUserAdapter.OnItemClickCallback {
            override fun onItemClicked(data: User) {
                showSelectedHero(data)
            }
        })
    }

    private fun showSelectedHero(user: User) {
        val detailUser = Intent(this@FavoriteActivity, DetailUserActivity::class.java)
        detailUser.putExtra(DetailUserActivity.USER_DATA, user)
        detailUser.putExtra(DetailUserActivity.IS_FROM_FAVORITE, true)
        startActivity(detailUser)
    }
}